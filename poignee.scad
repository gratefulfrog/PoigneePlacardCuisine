$fn = 100;

file = "poingee_01.dxf";
lay = "0";

ht = 20;
hDia = 3.2;

module get(){
  linear_extrude(ht)
    import(file,layer=lay);
}

module hole(){
  x = 15;
  z= 20;
  translate([x/2.,0,z/2.])
    rotate([90,0,0])
      cylinder(10,hDia,hDia,center=true);
}

difference(){
  get();
  hole();
}